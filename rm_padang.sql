-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 21, 2018 at 07:12 AM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rm_padang`
--

-- --------------------------------------------------------

--
-- Table structure for table `drinks`
--

CREATE TABLE `drinks` (
  `id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(200) NOT NULL,
  `avaiable` int(1) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drinks`
--

INSERT INTO `drinks` (`id`, `name`, `image`, `avaiable`, `content`) VALUES
(8, 'Es Teh Manis', 'esteh.jpg', 0, 'Es teh atau Teh es adalah teh yang didinginkan dengan es batu. Es teh seringkali ditambahkan rasa seperti melati, dan buah-buahan seperti limun, ceri, dan arbei, atau susu. Es teh adalah minuman yang sering diminum saat siang hari karena suhu udara yang panas. Di warteg, es teh sering diminum selain air dingin. Teh tarik adalah contoh dari es teh. Selain itu, beberapa merek juga menyediakan es teh, seperti Teh Botol, Frestea, dan Nu Green Tea'),
(9, 'Soda', 'soda.jpg', 0, 'Minuman berkarbonasi adalah minuman tidak memiliki kandungan alkohol yang mengalami proses karbonasi. Di seluruh belahan bumi, minuman berkarbonasi memiliki beberapa nama populer yang berbeda-beda, sebagai contoh, di Amerika Serikat, dikenal dengan nama soda, soda pop, pop atau tonik, di Inggris dikenal dengan fizzy drinks, di Kanada dikenal dengan Soda atau Pop saja. Sedangkan di daerah Ireland, mereka menyebutnya Minerals. '),
(10, 'Air Mineral Kemasan', 'aqua.jpg', 0, 'Air mineral adalah air yang mengandung mineral atau bahan-bahan larut lain yang mengubah rasa atau memberi nilai-nilai terapi. Banyak kandungan Garam, sulfur, dan gas-gas yang larut di dalam air ini. Air mineral biasanya masih memiliki buih. Air mineral bersumber dari mata air yang berada di alam.');

-- --------------------------------------------------------

--
-- Table structure for table `foods`
--

CREATE TABLE `foods` (
  `id` int(3) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(200) NOT NULL,
  `avaiable` int(1) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `foods`
--

INSERT INTO `foods` (`id`, `name`, `image`, `avaiable`, `content`) VALUES
(22, 'Ayam Bakar', 'ayam_bakar.jpg', 1, 'Makanan Indonesia yang terbilang simple dan sangat mudah untuk di buat dan di sajikan salah satunya adalah ayam bakar. Ya, makanan yang hampir bisa kita temui di semua daerah ini proses pembuatannya terbilang mudah. Bahkan bumbu yang di gunakan juga tidak terlalu banyak dan hanya menggunakan bumbu yang sudah biasa ada di dapur. Berbicara mengenai ayam bakar, akan semakin nikmat dan lezat jika di lengkapi dengan lalapan yang segar serta di suguhkan bersama sambal pedas dari cabai pilihan.'),
(23, 'Ayam Gulai', 'ayam_gulai.jpg', 0, 'Gulai ayam adalah hidangan tradisional Indonesia berupa ayam yang dimasak dengan saus yang pedas, kaya, kekuningan, seperti gulai . Ini berasal dari Sumatera Barat ( Padang ). Ini dapat digolongkan sebagai kari Indonesia . Bersama dengan gulai kambing, (kambing atau kambing gulai), ini adalah varian gulai yang paling umum dan populer.'),
(24, 'Ayam Kaleo', 'ayam_kalio.jpg', 0, 'Kalio adalah sebutan dari rendang setengah jadi, yang masih berwarna kecoklatan dibanding rendang yang berwarna cokelat kehitaman. Kalio bertekstur lengket dan basah dengan aroma karamel yang kuat, sementara rendang lebih kering, bertekstur kasar, dan mengeluarkan aroma rempah yang tajam. Umumnya kalio dijual sebagai rendang di Rumah Makan Padang di luar Sumatera Barat karena durasi memasak lebih pendek sehingga lebih praktis dan ekonomis untuk disajikan. Selain itu, rasanya tidak terlalu pedas dan daging yang dimasak juga tidak sekeras rendang. '),
(25, 'Ayam Balado', 'Ayam_Balado.jpg', 0, 'Balado adalah teknik memasak khas Minangkabau dengan cara menumis cabe giling dengan berbagai rempah, biasanya bawang merah, bawang putih, jeruk nipis. Berbeda dengan sambal lain yang hanya menggunakan cabe giling sebagai pendamping makanan dengan cara dicelup, balado dihidangkan dengan cara diimasak kembali dengan berbagai jenis masakan seperti dendeng, teri, ikan goreng, ayam goreng, bebek goreng, telur rebus, dan tempe goreng. Tindakan menambahkan cabe dan rempah dipercaya membuat makanan bertambah awet. '),
(26, 'Ayam Goreng', 'Ayam_Goreng.png', 0, 'Ayam goreng adalah hidangan yang dibuat dari daging ayam dicampur tepung bumbu yang digoreng dalam minyak goreng panas'),
(27, 'Gulai Otak', 'gulai_otak.jpg', 0, 'Makanan ini merupakan varian dari ayam gulai yang sama sama memakai bahan dasar gulai.'),
(28, 'Gulai Babat', 'gulai_babat.jpg', 0, 'Gulai Babat merupakan varian dari menu gulai yang menggunakan daging babat.'),
(29, 'Gulai Limpa', 'gulai_limpa.jpg', 0, 'Gulai Limpa merupakan varian lain dari gulai yang menggunakan limpa sebagai bahan makanannya.'),
(30, 'Gulai Usus', 'Gulai_usus.jpg', 0, 'Gulai usus adalah varian lain dari gulai yang menggunakan usus sebagain bahan dasarnya.'),
(31, 'Rendang', 'rendang.jpg', 0, 'Rendang atau randang adalah masakan daging bercita rasa pedas yang menggunakan campuran dari berbagai bumbu dan rempah-rempah. Masakan ini dihasilkan dari proses memasak yang dipanaskan berulang-ulang dengan santan kelapa. Proses memasaknya memakan waktu berjam-jam (biasanya sekitar empat jam) hingga kering dan berwarna hitam pekat. Dalam suhu ruangan, rendang dapat bertahan hingga berminggu-minggu.');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(3) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(1, 'test123', 'cc03e747a6afbbcbf8be7668acfebee5');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `drinks`
--
ALTER TABLE `drinks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `foods`
--
ALTER TABLE `foods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `drinks`
--
ALTER TABLE `drinks`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `foods`
--
ALTER TABLE `foods`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
