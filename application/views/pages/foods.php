<section class="portfolio" id="makanan">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Makanan</h2>
        <hr class="star-dark mb-5">
        <div class="row">
        <?php foreach ($data as $key){ ?>
          <div class="col-md-6 col-lg-4">
            <a class="portfolio-item d-block mx-auto" href="#detail-makanan-<?php echo $key['id'] ?>">
              <div class="portfolio-item-caption d-flex position-absolute h-100 w-100">
                <div class="portfolio-item-caption-content my-auto w-100 text-center text-white">
                  <i class="fa fa-search-plus fa-3x"></i>
                </div>
              </div>
              <img class="img-fluid" src="<?php echo base_url('uploads/'.$key['image']) ?>" alt="">
            </a>
          </div>
          <?php } ?>
        </div>
      </div>
    </section>

<?php foreach ($data as $key){ ?>
    <div class="portfolio-modal mfp-hide" id="detail-makanan-<?php echo $key['id']?>">
      <div class="portfolio-modal-dialog bg-white">
        <a class="close-button d-none d-md-block portfolio-modal-dismiss" href="#">
          <i class="fa fa-3x fa-times"></i>
        </a>
        <div class="container text-center">
          <div class="row">
            <div class="col-lg-8 mx-auto">
              <h2 class="text-secondary text-uppercase mb-0"><?php echo $key['name'] ?></h2>
              <?php if ($key['avaiable']==0){
                echo "Not Avaiable";
              }else{
                echo "Avaiable";
              } ?>
              <hr class="star-dark mb-5">
              <img class="img-fluid mb-5" src="<?php echo base_url('uploads/'.$key['image']) ?>" alt="">
              <p class="mb-5"><?php echo $key['content'] ?></p>
              <a class="btn btn-primary btn-lg rounded-pill portfolio-modal-dismiss" href="#">
                <i class="fa fa-close"></i>
                Tutup</a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php } ?>