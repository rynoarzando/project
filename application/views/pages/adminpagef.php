	<header class="masthead text-black text-center">
	  
	<!-- Foods -->
	<div class="container">
		<nav>
    <h3 align="center">Foods</h3><br /> 
      <div class="col-md-12 text-right" style="margin-bottom:20px">
        <a href="<?=base_url('adminrm/newfood')?>" class="btn btn-default "><i class="fa fa-plus" aria-hidden="true"></i>New Food</a>
      </div> 
           <div class="table-responsive">  
                <br />  
                <table id="foods_data" class="table table-bordered table-striped text-center">  
                     <thead>  
                          <tr>  
                               <th width="10%">Image</th>  
                               <th width="30%">Name</th>  
							                 <th width="30%">Content</th>
							                 <th width="10%">Avaiable</th>  
                               <th width="10%"></th>  
                               <th width="10%"></th>  
                          </tr>
                      </thead>
                        <tbody>
                          <?php
                              foreach ($data as $key){?>
                          <tr>  
                               <td width="10%"><img src="<?php echo base_url('uploads/'.$key['image']) ?>" class="img-thumbnail"></td>  
                               <td width="30%"><?php echo $key['name'] ?></td>  
							                 <td width="30%"><?php echo $key['content'] ?></td>
							                 <td width="10%"><a href="<?=base_url('admin/editAvaiablefon/'.$key['id']) ?>" class="btn btn-primary">On</a>
                               <a href="<?=base_url('admin/editAvaiablefoff/'.$key['id']) ?>" class="btn btn-danger">Off</a></td>  
                               <td width="10%"><a href="<?=base_url('admin/editFood/'.$key['id']) ?>" class="btn btn-primary">Edit<i class="fa fa-edit" aria-hidden="true"></i></a></td>  
                               <td width="10%"><a href="javascript:hapus('<?=$key['id']?>')" class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true">Delete</i></a></td>  
                          </tr>    
                     <?php } ?>
                        </tbody> 
                </table> 
           </div> 
		</nav>
	  </div>
    </header>
	
	