<div class="container-fluid" style="height: 150vh; ;background-size: cover; color: white;background-color: rgba(0,0,0,.5);background-blend-mode: multiply;">
<div class="container">
    <br>
    <br>
    <br>
<div class="row"  style="margin-top:70px">
   
   <div class="col-md-8 col-md-offset-2">
   <h1 class="text-uppercase mb-0">Edit Makanan</h1>
      <?php echo form_open_multipart('adminrm/editFoodprocess'.'/'.$foods->id) ?>
         <div class="form-group">
            <label for="name">Nama :</label>
            <input class="form-control" id="name" value="<?= $foods->name; ?>" name="name" />
         </div>
          <div class="form-group">
            <label for="image">Gambar :</label>
            <input class="form-control" type="file" name="userfile" value="<?= $foods->image; ?>" size="20" />
         </div>
         <div class="form-group">
            <label for="content">Konten :</label>
            <textarea class="form-control" id="content" name="content"><?= $foods->content; ?></textarea>
         </div>
         <div class="text-right">
            <a href="<?=base_url('adminrm/foods')?>" class="btn btn-warning">Kembali</a>
            <input type="submit" class="btn btn-primary" value="Edit">
         </div>
      <?php echo form_close(); ?>
   </div>
</div>
<hr>
</div>