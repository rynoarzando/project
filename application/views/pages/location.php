<section id="lokasi">
      <div class="container">
      <h2 class="text-center text-uppercase text-secondary mb-0">Lokasi Dan Kontak</h2>
        <hr class="star-dark mb-5">
        <div class="row">
          <div class="col-lg-8 mx-auto">
            <div class="embed-responsive embed-responsive-16by9">
          <iframe class="embed-responsive-item" src="<?=base_url('Home/maps')?>" width="700" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </div>

        <section class="portfolio" id="gallery">
      <div class="container">
        <h2 class="text-center text-uppercase text-secondary mb-0">Gallery</h2>
        <hr class="star-dark mb-5">
        <div class="row">
            <hr>
          <div class="col-md-6 col-lg-4">
            <img class="img-fluid" src="assets/img/rm1.jpg" alt="">
          </div>

          <div class="col-md-6 col-lg-4">
            <img class="img-fluid" src="assets/img/rm2.jpg" alt="">
          </div>

          <div class="col-md-6 col-lg-4">
            <img class="img-fluid" src="assets/img/rm3.jpg" alt="">
          </div>
            
        </div>
      </div>
    </section>


      </div>
    </section>