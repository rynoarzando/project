<section class="bg-primary text-white mb-0" id="paket">
      <div class="container">
        <h2 class="text-center text-uppercase text-white">Paket Box</h2>
        <hr class="star-light mb-5">
        <div class="row">
          <div class="col-lg-4 ml-auto">
          <img class="img-fluid" src="assets/img/nasibox1.jpg" alt="">
          <p class="lead">Paket Nasi Box 1</p>
            <p class="lead">Nasi</p>
            <p class="lead">Daun Singkong</p>
            <p class="lead">Nangka</p>
            <p class="lead">Sambal</p>
            <p class="lead">Lauk(Kikil, Cincang)</p>
          </div>
          <div class="col-lg-4 mr-auto">
          <img class="img-fluid" src="assets/img/nasibox2.jpg" alt="">
            <p class="lead">Paket Nasi Box 2</p>
            <p class="lead">Nasi</p>
            <p class="lead">Daun Singkong</p>
            <p class="lead">Nangka</p>
            <p class="lead">Sambal</p>
            <p class="lead">Lauk(Ayam, Rendang, Ikan)</p>
            <p class="lead">*Telur opsional</p>
          </div>
        </div>
        <div class="text-center mt-4">
          <a class="btn btn-xl btn-outline-light" href="#footer">
            <i class="fa fa-download mr-2"></i>
            Hubungi Dibawah
          </a>
        </div>
      </div>
    </section>