<footer>
    <div class="copyright py-4 text-center text-white">
      <div class="container">
        <small>Copytright &copy; <?=Date('Y')?> Ryno Arzando</small>
      </div>
    </div>
    </footer>
    <!-- Bootstrap core JavaScript -->
    <script src="<?=base_url()?>assets/vendor/jquery/jquery.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    
    <!-- Plugin JavaScript -->
    <script src="<?=base_url()?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>
    
    <!-- Custom scripts for this template -->
    <script src="<?=base_url()?>assets/js/freelancer.min.js"></script>
    
    <!-- Data Tables -->
    <script src="<?= base_url('assets/js/jquery-3.2.1.min.js')?>"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url('assets/js/dataTables.bootstrap.min.js')?>"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
      $('#drinks_data').dataTable();
      function hapus(id){
      swal({
        title	: "Peringatan",
        text	: "Apakah anda yakin menghapus data?",
        icon	: "warning",
        buttons: true,
        dangerMode: true,
      }).then(function(e){
        if(e.value){
          swal("Penghapusan dibatalkan",{
            icon: "success"
          });
        }else{
          window.location="<?=base_url('admin/deleteDrink/')?>"+id;
          swal("Penghapusan berhasil",{
            icon: "success"
          });
        }
      });
    }
  </script>

    