<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

class Admin extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('status') != "login"){
			redirect(base_url("login"));
		}

		$this->load->model(array(
			'Crud_foods' => 'foods',
			'Crud_drinks'=> 'drinks'
		));
		$this->load->library('form_validation');

	}

	public function indexf(){
		$data['data'] = $this->db->get('foods')->result_array();
		$this->load->view('include/headera');
		$this->load->view('pages/adminpagef',$data);
		$this->load->view('include/footeraf');
	}

	public function newFood()
	{	
		$this->form_validation->set_rules('name','image','required');

		if ($this->form_validation->run() == FALSE){
		$this->load->view('include/headera');
		$this->load->view('pages/newfood');
		$this->load->view('include/footeraf');
		}else{

			$data['name'] = $this->input->post('name');
			$data['content'] = $this->input->post('content');

			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'jpg|gif|png';
			$config['max_size'] = '10000';
			$config['max_width'] = '5000';
			$config['max_height'] = '5000';

			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				var_dump($error); die();
			}else{

				$upload = $this->upload->data();
				$data['image'] = $upload['file_name'];
				$this->foods->input_data($data);
				redirect('adminrm/foods');
		}	
		}
	}

	public function deleteFood($id)
	{
		$this->foods->deleteFoods($id);
		redirect ('adminrm/foods');
	}

	public function editFood($id)
	{
		$data['foods'] =$this->foods->getFoods($id);
		$this->load->view('include/headera');
		$this->load->view('pages/editfood',$data);
		$this->load->view('include/footeraf');
	}

	function editFoodprocess($id)
	{
		$this->form_validation->set_rules('name','content','required');

		if ($this->form_validation->run() == FALSE){
		$this->load->view('include/headera');
		$this->load->view('pages/editfood');
		$this->load->view('include/footeraf');
		}else{

			$data['name'] = $this->input->post('name');
			$data['content'] = $this->input->post('content');

			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'jpg|gif|png';
			$config['max_size'] = '10000';
			$config['max_width'] = '5000';
			$config['max_height'] = '5000';

			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload())
			{
				$this->foods->updateFoods($data,$id);
				redirect('adminrm/foods');

			}else{
				$upload = $this->upload->data();
				$data['image'] = $upload['file_name'];
			}
				$this->foods->updateFoods($data,$id);
				redirect('adminrm/foods');
		}
	}

	public function editAvaiablefon($id)
	{
		$data['avaiable'] = '1'; 
		$this->foods->updateFoods($data,$id);
		redirect('adminrm/foods');
	}
	public function editAvaiablefoff($id)
	{	
		$data['avaiable'] = '0';
		$this->foods->updateFoods($data,$id);
		redirect('adminrm/foods');
	}

	public function indexd(){
		$data['data'] = $this->db->get('drinks')->result_array();
		$this->load->view('include/headera');
		$this->load->view('pages/adminpaged',$data);
		$this->load->view('include/footerad');
	}

	public function newDrink()
	{
		$this->form_validation->set_rules('name','image','required');

		if ($this->form_validation->run() == FALSE){
		$this->load->view('include/headera');
		$this->load->view('pages/newdrink');
		$this->load->view('include/footerad');
		}else{

			$data['name'] = $this->input->post('name');
			$data['content'] = $this->input->post('content');

			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'jpg|gif|png';
			$config['max_size'] = '10000';
			$config['max_width'] = '5000';
			$config['max_height'] = '5000';

			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload())
			{
				$error = array('error' => $this->upload->display_errors());
				var_dump($error); die();
			}else{

				$upload = $this->upload->data();
				$data['image'] = $upload['file_name'];
				$this->drinks->input_data($data);
				redirect('adminrm/drinks');
		}	
		}
	}

	public function deleteDrink($id)
	{
		$this->drinks->deleteDrinks($id);
		redirect('adminrm/drinks');
	}

	public function editDrink($id)
	{
		$data['drinks'] =$this->drinks->getDrinks($id);
		$this->load->view('include/headera');
		$this->load->view('pages/editdrink',$data);
		$this->load->view('include/footerad');
	}

	function editDrinkprocess($id)
	{
		$this->form_validation->set_rules('name','content','required');

		if ($this->form_validation->run() == FALSE){
		$this->load->view('include/headera');
		$this->load->view('pages/editdrink');
		$this->load->view('include/footerad');
		}else{

			$data['name'] = $this->input->post('name');
			$data['content'] = $this->input->post('content');

			$config['upload_path'] = './uploads/';
			$config['allowed_types'] = 'jpg|gif|png';
			$config['max_size'] = '10000';
			$config['max_width'] = '5000';
			$config['max_height'] = '5000';

			$this->upload->initialize($config);
			
			if ( ! $this->upload->do_upload())
			{
				$this->drinks->updateDrinks($data,$id);
				redirect('adminrm/drinks');

			}else{
				$upload = $this->upload->data();
				$data['image'] = $upload['file_name'];
			}
				$this->drinks->updateDrinks($data,$id);
				redirect('adminrm/drinks');
		}
	}

	public function editAvaiabledon($id)
	{
		$data['avaiable'] = '1'; 
		$this->drinks->updateDrinks($data,$id);
		redirect('adminrm/drinks');
	}
	public function editAvaiabledoff($id)
	{	
		$data['avaiable'] = '0';
		$this->drinks->updateDrinks($data,$id);
		redirect('adminrm/drinks');
	}
}