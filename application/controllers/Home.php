<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
    function __construct(){
        parent::__construct();
    }

    public function index()
    {
        $this->header();
        $this->makanan();
        $this->minuman();
        $this->paket();
        $this->lokasi();
        $this->footer();
    }

    public function makanan()
    {
        $data['data'] = $this->db->get('foods')->result_array(); 
        $this->load->view('pages/foods',$data);
    }

    public function minuman()
    {   
        $data['data'] = $this->db->get('drinks')->result_array(); 
        $this->load->view('pages/drinks',$data);
    }

    public function paket()
    {
        $this->load->view('pages/packets');
    }

    public function header()
    {
        $this->load->view('include/header');
    }
    public function footer()
    {
        $this->load->view('include/footer');
    }

    public function lokasi()
    {
        $this->load->view('pages/location');
    }

    public function maps()
    {
        $this->load->view('pages/maps');
    }
}
