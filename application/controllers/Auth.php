<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_login');
    }

    public function index()
    {
        $this->load->view('pages/login');
    }

    function login_action()
    {
        $username = $this->input->post('username');
		$password = $this->input->post('password');
		$where = array(
			'username' => $username,
			'password' => md5($password)
            );

        $cek = $this->M_login->login_check("user",$where)->num_rows();
        
		if($cek > 0){
 
			$data_session = array(
				'nama' => $username,
				'status' => "login"
				);
 
			$this->session->set_userdata($data_session);
			redirect(base_url("adminrm/foods"));
		}else{
            $this->session->set_flashdata('failLogin', 'Username or Password are incorrect');
	    		redirect(base_url("adminrm"));
		}
    }

    function logout()
    {
        $this->session->sess_destroy();
		redirect(base_url('adminrm'));
    }
}