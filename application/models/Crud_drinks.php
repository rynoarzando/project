<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_drinks extends CI_Model  {
    public function input_data($data)
    {
        return $this->db->insert('drinks',$data);
    }

	public function getDrinks($id)
	{
		$query = $this->db->get_where('drinks',array('id'=>$id));
		return $query->row();
	}

	public function updateDrinks($data,$id)
	{
		$this->db->where('id',$id);
		return $this->db->update('drinks',$data);
	}

	public function deleteDrinks($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('drinks');
	}
} 