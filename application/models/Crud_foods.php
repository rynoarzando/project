<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Crud_foods extends CI_Model  {
    public function input_data($data)
    {
        return $this->db->insert('foods',$data);
    }

	public function getFoods($id)
	{
        $query = $this->db->get_where('foods',array('id'=>$id));
		return $query->row();
	}

	public function updateFoods($data,$id)
	{   
        $this->db->where('id',$id);
		return $this->db->update('foods',$data);
	}

	public function deleteFoods($id)
	{
		$this->db->where('id',$id);
		return $this->db->delete('foods');
	}
} 